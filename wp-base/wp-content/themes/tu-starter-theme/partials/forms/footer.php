
	<div class="_10footer">
		<div class="up">
			<h4>đối tác chiến lược</h4>
			<div class="tranparent">
				<div class="nho"></div>
				<div class="btnleft">
					<i class="fa fa-angle-left"></i>
				</div>
				<div class="btnright">
					<i class="fa fa-angle-right"></i>
				</div>
			</div>

			<div class="imgs">	
				<img src="<?php echo IMAGE_URL.'../../images/91.jpg'; ?>">	
				<img src="<?php echo IMAGE_URL.'../../images/91.jpg'; ?>">	
				<img src="<?php echo IMAGE_URL.'../../images/91.jpg'; ?>">	
				<img src="<?php echo IMAGE_URL.'../../images/91.jpg'; ?>">	
				<img src="<?php echo IMAGE_URL.'../../images/91.jpg'; ?>">	
				<img src="<?php echo IMAGE_URL.'../../images/91.jpg'; ?>">	
				<img src="<?php echo IMAGE_URL.'../../images/91.jpg'; ?>">	
				<img src="<?php echo IMAGE_URL.'../../images/91.jpg'; ?>">	
			</div>		

		</div>

		<div class="center">
			<div class="content">
				<div class="_1">
					<h6>dịa chỉ vp bán hàng và nhà mẫu</h6>
					<p class="nau">Số 09, đường Phạm Hùng, quận Nam Từ Liêm, Hà Nội</p>
				</div>
				<div class="_2">
					<div class="k1">
						<h6>dịa chỉ dự án:</h6>
						<p class="nau">Số 02, phố Tôn Thất Thuyết, quận Cầu Giấy, Hà Nội</p>
					</div>
					<div class="k1">
						<h6>email:</h6>
						<p class="nau">info@skypark.com.vn</p>
					</div>
					<div class="k1">
						<h6>số điện thoại</h6>
						<p class="nau">0988 888 888</p>
					</div>
				</div>
			</div>

			<p class="note">* Hình ảnh chỉ mang tính chất minh họa. Căn hộ và các trang thiết bị kèm theo căn hộ sẽ được bàn giao theo đúng quy định tại Hợp Đồng  Mua Bán ký kết.</p>
		</div>

		<div class="down">
			<div class="container">
				<div class="float-sm-left">
					<i class="fa fa-phone"></i>
					<i class="fa fa-envelope"></i>
					<i class="fa fa-facebook "></i>
				</div>
				<div class="float-sm-right">© Sky Park 2017, All Rights Reserved.</div>
			</div>
		</div>
	</div> <!-- end footer  -->

	<i class="fa fa-long-arrow-up wow bounce" data-wow-duration="3s" data-wow-iteration="10000"></i>