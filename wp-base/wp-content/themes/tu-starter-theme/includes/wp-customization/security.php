<?php

// hide version wordpress
function wpb_remove_version() {
    return '';
}

add_filter('the_generator', 'wpb_remove_version');

remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head, 10, 0');

//Limit Login Wordpress
if (!class_exists('Limit_Login_Attempts')) {

    class Limit_Login_Attempts {

        var $failed_login_limit = 5;                    //Number of authentification accepted
        var $lockout_duration = 6000;                 //Stop authentification process for 30 minutes: 60*30 = 1800
        var $transient_name = 'attempted_login';    //Transient used

        public function __construct() {
            add_filter('authenticate', array($this, 'check_attempted_login'), 30, 3);
            add_action('wp_login_failed', array($this, 'login_failed'), 10, 1);
        }

        /**
         * Lock login attempts of failed login limit is reached
         */
        public function check_attempted_login($user, $username, $password) {
            if (get_transient($this->transient_name)) {
                $datas = get_transient($this->transient_name);

                if ($datas['tried'] >= $this->failed_login_limit) {
                    $until = get_option('_transient_timeout_' . $this->transient_name);
                    $time = $this->when($until);
//Display error message to the user when limit is reached 
                    return new WP_Error('too_many_tried', sprintf(__('<strong>Lỗi</strong>: Xin vui lòng thử lại sau %1$s.'), $time));
                }
            }

            return $user;
        }

        /**
         * Add transient
         */
        public function login_failed($username) {
            if (get_transient($this->transient_name)) {
                $datas = get_transient($this->transient_name);
                $datas['tried'] ++;

                if ($datas['tried'] <= $this->failed_login_limit)
                    set_transient($this->transient_name, $datas, $this->lockout_duration);
            } else {
                $datas = array(
                    'tried' => 1
                );
                set_transient($this->transient_name, $datas, $this->lockout_duration);
            }
        }

        /**
         * Return difference between 2 given dates
         * <a href="/param">@param</a>  int      $time   Date as Unix timestamp
         * @return string           Return string
         */
        private function when($time) {
            if (!$time)
                return;

            $right_now = time();

            $diff = abs($right_now - $time);

            $second = 1;
            $minute = $second * 60;
            $hour = $minute * 60;
            $day = $hour * 24;

            if ($diff < $minute)
                return floor($diff / $second) . __(" giây");

            if ($diff < $minute * 2)
                return __("sau 1 phút");

            if ($diff < $hour)
                return floor($diff / $minute) . __(" phút");

            if ($diff < $hour * 2)
                return __("sau 1 giờ");

            return floor($diff / $hour) . __(" giờ");
        }

    }

}

//Enable it:
new Limit_Login_Attempts();

////Defaul message error login wordpress
function no_wordpress_errors() {
    return __('Lỗi đăng nhập rồi, xin vui lòng liên hệ với người quản trị website.', TEXT_DOMAIN);
}

add_filter('login_errors', 'no_wordpress_errors');