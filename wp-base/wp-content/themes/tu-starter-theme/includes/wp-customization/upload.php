<?php

/** Remove accents from file name when uploading */
add_filter('sanitize_file_name', 'sanitize_file_name_remove_accents', 10);

function sanitize_file_name_remove_accents($filename) {
    return remove_accents($filename);
}

/* Change upload url */

add_filter('wp_handle_upload_prefilter', 'handle_upload_prefilter');
add_filter('wp_handle_upload', 'handle_upload');

function handle_upload_prefilter($file) {
    add_filter('upload_dir', 'custom_upload_dir');
    return $file;
}

function handle_upload($fileinfo) {
    remove_filter('upload_dir', 'custom_upload_dir');
    return $fileinfo;
}

function custom_upload_dir($path) {
    /*
     * Determines if uploading from inside a post/page/cpt - if not, default Upload folder is used
     */
    $use_default_dir = ( isset($_REQUEST['post_id']) && $_REQUEST['post_id'] == 0 ) ? true : false;
    if (!empty($path['error']) || $use_default_dir)
        return $path; // Error: not uploading from a post/page/cpt 

    $the_post = get_post($_REQUEST['post_id']);
    $y = date('Y', strtotime($the_post->post_date));
    $m = date('m', strtotime($the_post->post_date));
    $d = date('d', strtotime($the_post->post_date));

    $customdir = '/';

    $path['path'] = str_replace($path['subdir'], '', $path['path']); //remove default subdir (year/month)
    $path['url'] = str_replace($path['subdir'], '', $path['url']);
    $path['subdir'] = $customdir;
    $path['path'] .= $customdir;
    $path['url'] .= $customdir;

    return $path;
}
